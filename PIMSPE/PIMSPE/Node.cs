﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIMSPE
{
    public class Leg
    {
        public string color { get; set; }
        public int height { get; set; }
        public string reference { get; set; }
    }

    public class Node
    {
        public string besoLuxRef { get; set; }
        public string producerRel { get; set; }
        public string brandRel { get; set; }
        public string titleForLabels { get; set; }
        public object producersReference { get; set; }
        public object producersPrice { get; set; }
        public object ean { get; set; }
        public string packagesQuantity { get; set; }
        public string fColor { get; set; }
        public int? numberOfSeats { get; set; }
        public object fabrics { get; set; }
        public List<Leg> legs { get; set; }
        public string totalWeight { get; set; }
        public string totalVolume { get; set; }
        public string category { get; set; }
        public string family { get; set; }
        public object hsCode { get; set; }
        public string legsPlacement { get; set; }
        public object legsNumber { get; set; }
    }

    public class Edge
    {
        public Node node { get; set; }
    }

    public class GetProductListing
    {
        public List<Edge> edges { get; set; }
    }

    public class Root
    {
        public GetProductListing getProductListing { get; set; }
    }
}
